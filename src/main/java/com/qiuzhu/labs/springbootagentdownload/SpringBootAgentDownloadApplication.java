package com.qiuzhu.labs.springbootagentdownload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAgentDownloadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAgentDownloadApplication.class, args);
	}
}
