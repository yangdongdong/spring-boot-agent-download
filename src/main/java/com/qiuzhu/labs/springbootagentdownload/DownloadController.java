package com.qiuzhu.labs.springbootagentdownload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping; 
@Controller
public class DownloadController {
	private static final Logger log = Logger.getLogger(DownloadController.class);

	@GetMapping("/download")
	public void download(String url,String filename,HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException {
		log.info("request for download");
//		long startTime = System.nanoTime();
		// 请求的文件名
		HttpClient httpClient = HttpClientBuilder.create().build();
		// 后端服务器的文件地址
		HttpGet httpGet = new HttpGet(url);
		log.info("distribute file to " + url);
		HttpResponse backResponse;
		try {
			backResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = backResponse.getEntity();
			InputStream in = httpEntity.getContent();
			byte[] buf = new byte[4096];
			int readLength;
			response.setCharacterEncoding("UTF-8");
			// 文件名转码
			filename = URLEncoder.encode(filename, "UTF-8");
			
			// 告诉客户端下载文件
			if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > -1) {
				response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + filename);
			} else {
				response.setHeader("content-disposition", "attachment; filename=" + filename);
			}

			
			ServletOutputStream out = response.getOutputStream();
			while ((readLength = in.read(buf)) != -1) {
				out.write(buf, 0, readLength);
			}
			in.close();
			out.flush();
			out.close();
		} catch (ClientProtocolException e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			log.error("ClientProtocolException when redirect bigfile. " + sw.toString());
		} catch (IOException e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			log.error("IOException when redirect bigfile. " + sw.toString());
		}
//		long endTime = System.nanoTime();
//		System.out.println("Response time: " + (endTime - startTime) + " ns");
	}

}
